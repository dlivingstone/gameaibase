#include "rect.h"
#include <SDL_opengl.h>

Rect::Rect(void) : xpos(0.0f), ypos(0.0f), width(0.05f), height(0.05f)
{
}

Rect::Rect(float x, float y, float w, float h) : xpos(x), ypos(y), width(w), height(h) 
{
}

Rect::~Rect(void)
{
}


void Rect::draw(void) {

	glBegin(GL_POLYGON);
	  glVertex3f (xpos-width/2, ypos-height/2, 0.0); // first corner
	  glVertex3f (xpos+width/2, ypos-height/2, 0.0); // second corner
	  glVertex3f (xpos+width/2, ypos+height/2, 0.0); // third corner
	  glVertex3f (xpos-width/2, ypos+height/2, 0.0); // fourth corner
	glEnd();
}