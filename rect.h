#pragma once
class Rect
{
public:
	Rect();
	Rect(float x, float y, float w, float h);
	~Rect(void);

	float x() { return xpos; }
	float y() { return ypos; }
	void setX(float x) { xpos = x; }
	void setY(float y) { ypos = y; }
	float w() { return width; }
	float h() { return height; }
	void setW(float w) { width = w; }
	void setH(float h) { height = h; }

	void draw(void);
private:
	float xpos;
	float ypos;
	float width;
	float height;
};

